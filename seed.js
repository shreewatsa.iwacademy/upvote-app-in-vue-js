const submissions = [
    {
        id:1,
        title: 'Yellow Pail',
        description : ' On-demand castle construction expertise.',
        url: '#',
        votes: 16,
        avatar: './public/avatar/avatar.jpg',
        submissionImage: './public/images/submission.png',
    },
    {
        id:2,
        title: 'O:M Veg Momo',
        description : 'Best veg momo in town.',
        url: '#',
        votes: 21,
        avatar: './public/avatar/avatar.jpg',
        submissionImage: './public/images/submission.png',
    },
    {
        id: 3,
        title: 'Supermajority: The Fantasy Congress League',
        description: 'Earn points when your favorite politicians pass legislation.',
        url: '#',
        votes: 11,
        avatar: './public/avatar/avatar.jpg',
        submissionImage: './public/images/submission.png',
      },
      {
        id: 4,
        title: 'Tinfoild: Tailored tinfoil hats',
        description: 'We have your measurements and shipping address.',
        url: '#',
        votes: 17,
        avatar: './public/avatar/avatar.jpg',
        submissionImage: './public/images/submission.png',
      },
      {
        id: 5,
        title: 'Haught or Naught',
        description: 'High-minded or absent-minded? You decide.',
        url: '#',
        votes: 9,
        avatar: './public/avatar/avatar.jpg',
        submissionImage: './public/images/submission.png',
      }
]

const Seed = {
    submissions : submissions
}


// window.Seed = (function () {
//     const submissions = [
//       {
//         id: 1,
//         title: 'Yellow Pail',
//         description: 'On-demand sand castle construction expertise.',
//         url: '#',
//         votes: 16,
//         avatar: '../public/images/avatars/daniel.jpg',
//         submissionImage: '../public/images/submissions/image-yellow.png',
//       },
      
//     ];
  
//     return { submissions: submissions };
//   }());