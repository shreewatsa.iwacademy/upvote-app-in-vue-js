const submissionComponent = {
  template:
    `
    <div style="display:flex; width:100%;">
    <figure class="media-left">
        <p class="image is-64x64">
            <!-- https://bulma.io/images/placeholders/128x128.png -->
            <img :src="submission.submissionImage">
        </p>
    </figure>
    <div class="media-content">
        <div class="content">
            <p>
                <strong>
                    <a href="#" class="has-text-info">{{ submission.title }}</a>
                    <span class='tag is-small'>#{{ submission.id }}</span>
                </strong>
                <br>
                <br>
                {{ submission.description }}
                <br>
                <small class="is-size-7">
                    Submitted by: <img :src="submission.avatar" alt="" class='image is-24x24'>
                </small>
            </p>
        </div>
    </div>
    <div class="media-right">
        <span class="icon is-large" @click="upvote(submission.id)">
            <i class="fa fa-chevron-up"> </i>
            <strong class='has-text-info'> {{ submission.votes }}</strong>
        </span>
        </span>
    </div>
    </div>
  `,
  props: ['submission','submissions'],
  methods: {
    upvote(submissionId) {
      const submission = this.submissions.find(submission => submission.id === submissionId);
      submission.votes++;
    },
  }
}



var app = new Vue({
  el: '#app',
  data: {
    message: 'Vue is working !!',
    submissions: Seed.submissions
  },
  computed: {
    sortedSubmissions() {
      return this.submissions.sort((a, b) => {
        return b.votes - a.votes
      });
    }
  },
  components:{
    "submission-component": submissionComponent
  }
});